# MTRN2500 2022T3 - Assignment 2

# Change Logs

Changes of the assignment specification will be written here.

- `2022/11/26`: Fixed initial mode output again
- `2022/11/17`: Fixed initial mode output
- `2022/11/16`: Fix directory structures for starter files and submission
- `2022/11/13`: Clarify `StaffRobot` output
- `2022/11/12`: Removed reference to given `BaseRobot` main function.
- `2022/11/12`: Added clarification to `BaseRobot::move()`.
- `2022/11/10`: Clarify `x` and `z` is used to adjust motor speed instead of `+` and `-`.
- `2022/11/01`: Fix instantiation of `CustomerRobot` and `StaffRobot`.

# Table of Contents

[TOC]

# 1. Overview

The C++ assignment of MTRN2500 22T3 is a robotics simulation project based on the Webots simulator. In the assignment, you will be given a Webots world file containing multiple robots in a café setup. You are required to develop multiple controllers using C++.

The assignment is worth **35%** of your total marks in this course: **25%** is on the program’s correctness (functionality), **3%** is on the state diagram, **1%** is on the progress check,  and **6%** is on the style (quality). Unless otherwise announced, the due date of this assignment is **1:00pm 28th November 2022 (Monday Week 12)**. You should complete this assignment **individually**.

## 1.1. Expectations

By the end of the assignment, you are expected to have been able to:

- Understand the basic features of C++ and use them appropriately in developing robot controllers
- Use Object-Oriented Programming techniques to design C++ programs
- Use State Diagram to describe the behaviour of the system
- Make appropriate use of standard libraries (including STL)
- Be familiar with file I/O
- Follow good programming styles and practices
- Critically search, read and summarise online documentation and apply the relevant knowledge to project

## 1.2. Learning Outcomes Associated with this Assignment:

- **LO1**: Demonstrate effective use of CPP programming to solve problems in relation to Mechatronic applications.
- **LO2**: Recognise and follow modern best practices in CPP programming.
- **LO3**: Explain the concepts within Object-Oriented Programming and apply these concepts to C++ program design.
- **LO4**: Generating interfaces for an external device through a computer program to effect control action

# 2. Task Description

Background: A director wanted to shoot a short movie on the workflow of a café. Before taking the actual shot, the director would like to build a simulation for the scenario first. Please develop a robotics simulation to help the director.

In this assignment, you will be given a Webots world file, shown [below](README/setup_MTRN2500_wbt.png). The setup contains four Customer Robots (CRs, on the left), one Staff Robot (SR, on the right), and one Director Robot (DR, behind the scenes). This assignment is to simulate the workflow of an operating cafe. The CRs are responsible for buying coffees, the SR is responsible for making and selling coffees, and the DR is responsible for providing user interfaces as well as directing the activities of the CRs and the SR.

![Webots World Setup](README/setup_MTRN2500_wbt.png)

## 2.1. Assignment Files

When you open the assignment folder, you should see the following:

```
z1234567_MTRN2500
├── Account.csv
├── Menu.csv
├── Order.csv
├── Starting.csv
├── controllers
│   ├── CustomerRobotMain
│   │   ├── CustomerRobotMain.cpp
│   │   └── Makefile
│   └── StaffRobotMain
│       ├── Makefile
│       └── StaffRobotMain.cpp
└── worlds
    └── MTRN2500.wbt

5 directories, 11 files
```

# 3. Robots

The staff robot (SR) and the customer robot (CR) are the "In front of the scene" robots. They will be in an idle state until they receive the command from the director robot. The implementation for these controllers must be Object-Orientated Programming. The images for the SR and CRs are shown below. [Table 1 (Section 3.1)](#31-table-1-device-characteristics-and-values) summaries the robots’ details.

![Staff and Customer Robots](README/staff_customer_robots.png)

## 3.1. Table 1: Device Characteristics and Values

| **Devices**     | Characteristics            | Values              |
| --------------- | -------------------------- | ------------------- |
| Motor           | Left Motor Name            | `left wheel motor`  |
| Motor           | Right Motor Name           | `right wheel motor` |
| Motor           | Maximum Speed              | 6.28 rad/s          |
| Emitter         | Emitter Name               | `emitter`           |
| Receiver        | Receiver Name              | `receiver`          |
| GPS             | GPS Name                   | `gps`               |
| Compass         | Compass Name               | `compass`           |
| Distance Sensor | Left Distance Sensor Name  | `left eye`          |
| Distance Sensor | Right Distance Sensor Name | `right eye`         |
| Position Sensor | Left Position Sensor Name  | `left wheel sensor` |
| Position Sensor | Right Position Sensor Name | `right wheel sensor`|

## 3.2. `BaseRobot`

The same robot model is used for all the "In front of the scene" robots. To control them, you are required to create a `BaseRobot` class composed of the Webots’s robot class consisting of the following general information. You are allowed to include any helper functions or member variables. In this section, you are required to develop a `z1234567BaseRobot.hpp` and a `z1234567BaseRobot.cpp` replacing `z1234567` with your zID.

In order to test your implementation of BaseRobot, you should create a controller that uses all the methods in varying ways.

### 3.2.1. Implementation of `BaseRobot` Class

<table>
  <tr>
    <th>Method</th>
    <th>Description</th>
    <th>Usage</th>
    <th>Output</th>
    <th>Exception</th>
  </tr>
  <tr>
    <td><code>BaseRobot()</code></td>
    <td>
      A constructor that initializes the robot, auto assign an ID and initializes the onboard
      sensors & actuators.
      <br />
      Base Robot ID:
      <ul>
        <li>Purple: 1</li>
        <li>White: 2</li>
        <li>Gold: 3</li>
        <li>Green: 4</li>
        <li>Staff: 5</li>
      </ul>
      Use the name of the robot inside to assign the automatic ID. See the <a href="https://cyberbotics.com/doc/reference/robot" target="_blank">docs</a> for the method.
    </td>
    <td><code>BaseRobot robot;</code></td>
    <td><code>Robot {robotID} has been created.</code></td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>~BaseRobot()</code></td>
    <td>Destructor</td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>void move(double, double, double)</code></td>
    <td>
      Move to the input location. The inputs are <i>x</i>, <i>y</i>, <i>heading</i> in [meter, meter, radian]. The
      robot should move to the position <i>x</i>, <i>y</i>, and turn <i>heading</i> radians relative to the starting
      position and heading of the robot.
    </td>
    <td><code>BaseRobot robot;</code><br /><code>robot.move(0.5,3.0,0.0); </code></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>void sendMessage(const std::string&, int)</code></td>
    <td>Send a message string to the robot. The inputs are message string and the robotID</td>
    <td>
      <code>BaseRobot robot;<br />std::string str{"hi"};<br />robot.sendMessage(str,2); </code>
    </td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>std::string receiveMessage()</code></td>
    <td>
      Receive the message from other robots and return the message. If no message received, return
      an empty string.
    </td>
    <td>
      <code>BaseRobot robot;<br />std::string str{robot.receiveMessage()};</code>
    </td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td>
      <code>friend std::ostream& operator&lt;&lt;(std::ostream&, BaseRobot const&)</code>
    </td>
    <td>
      Prints the state of the robot in meters and radians.<br />
      The position and orientation of the robot should round to 3 decimal places.
    </td>
    <td>
      <code>BaseRobot robot;<br />std::cout &lt;&lt; robot &lt;&lt; std::endl;</code>
    </td>
    <td>
      <code>The robot {robotID} is currently at [{x},{y}] facing {angle}.</code>
    </td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void run() = 0</code></td>
    <td>
      A pure virtual function. Subclasses will have to implement this member function.
    </td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void remoteControl() = 0</code></td>
    <td>A pure virtual function. Subclasses will have to implement this member function. Refer to <a href="#333-auto-mode">Section 3.3.3 Auto mode</a></td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void autoMode() = 0</code></td>
    <td>A pure virtual function. Subclasses will have to implement this member function. Refer to <a href="#333-auto-mode">Section 3.3.3 Auto mode</a></td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
</table>

## 3.3. `StaffRobot` & `CustomerRobot`

In this section, you are required to create **two derived classes**, `StaffRobot` Class & `CustomerRobot` Class, using the `BaseRobot` Class developed. In those classes, you are only required to develop 3 modes introduced in the later sections. You are encouraged to review the functions in the `BaseRobot` class to develop these features.

You are required to develop the following

- `z1234567StaffRobot.hpp`
- `z1234567StaffRobot.cpp`

for the `StaffRobot` Class and the following

- `z1234567CustomerRobot.hpp`
- `z1234567CustomerRobot.cpp`

for the `CustomerRobot` Class, replacing `z1234567` with your zID.

We have provided main functions for `CustomerRobot` and `StaffRobot`. You are not allowed to modify the main file other than including your implemented header files.

### 3.3.1. Initial Mode

Under this mode, both the CRs and SR will be waiting for commands from the DR. When it receives the command from DR, it should enter either remote control mode or auto mode. See [Section 3.4.1](#341-initial-mode) for details.

### 3.3.2. Robot Control Mode

When the DR receives `R` or `r` in the initial mode and recognizes the robot selection command, it should set the selected robot (the SR or one of the four CRs) to the remote control mode.
The controllers can assume only one robot needs to be controlled, from the DR receives a command to enter the remote control mode, until the end of the whole simulation. That is, no reselecting robot will happen before the simulation is restarted.

Under the remote control mode, the user must manually control the selected robot using the keyboard. For different robots, the control commands are different (see [Table 2 (Section 3.3.2.1)](#3321-table-2-control-commands-in-the-remote-control-mode)):

- For the CRs, the `WAD` (uppercase or lowercase) keys are used;
- For the SR, the keyboard arrows are used;
- When one of the above keys is pressed, the robot starts moving with each motor running at `0.5 * MaxMotorSpeed`;
- The robot will continue executing the current command until the next command is received
- `x` / `z` will increase/decrease the absolute motor speed by `0.1 * MaxMotorSpeed`, capped by `0` and `MaxMotorSpeed`;
- When the Space key is pressed, the robot stops
- When the `E` or `e` key is pressed, the mode is completed and all the controllers should exit the simulation.

#### 3.3.2.1. Table 2: Control commands in the remote control mode

| Control Methods                                        | Customer Robot | Staff Robot |
| ------------------------------------------------------ | -------------- | ----------- |
| Move Forward                                           | `W` or `w`     | Up Arrow    |
| Move Backward                                          | `S` or `s`     | Down Arrow  |
| Spin Left                                              | `A` or `a`     | Left Arrow  |
| Spin Right                                             | `D` or `d`     | Right Arrow |
| Stop                                                   | Space          | Space       |
| Increase absolute motor speed by `0.1 * MaxMotorSpeed` | `X` or `x`     | `X` or `x`  |
| Decrease absolute motor speed by `0.1 * MaxMotorSpeed` | `Z` or `Z`     | `Z` or `z`  |
| Exit Remote Control Mode                               | `E` or `e`     | `E` or `e`  |

### 3.3.3. Auto mode

When the DR receives `A` or `a` in the initial mode, it should set all the robots (one SR and four CRs) to the auto mode. The four CRs will start buying coffee, and the SR will start making and selling coffee, all under the direction of the DR.

The auto mode should fulfil the following criteria:

#### 3.3.3.1. Settings

- **Only** the SR and CR can access "Starting.csv".
- **Only** the DR can access "Order.csv".
- **Only** the SR can access to "Menu.csv".
- **Only** the SR can access to "Account.csv".
- **The data in the csv file shouldn’t be shared other than the order.**

#### 3.3.3.2. Director Robot

- Informing the CR stated what it should order, assuming CR is given one order at a time.
- When the CR and the SR have conducted an order, the DR should either inform the relevant robot to conduct another order or end the process when all orders in the "Order.csv" have been performed.

#### 3.3.3.3. Customer Robot

- When it receives the order details from the DR, it should transverse to the "OrderTileCR".
- It then should communicate with SR to check
  - Is the order on the menu?
  - Does it have enough money to purchase it?
- The order should **ONLY** be made if those criteria are fulfilled. If any of those criteria aren’t fulfilled, the order is cancelled, and the CR returns to the starting position. The order ends.
- If the order is made, the CR should
  - Deduct the price of the order from their balance.
  - Wait their order at any location, until the SR completes its order.
- When the order is ready, CR should proceed to the "PickupTileCR" to pick up the order.
- Proceed back to the starting position and the order is completed.

#### 3.3.3.4. Staff Robot

- When it receives order details from CR, it should transverse to the "OrderTileSR".
- It should communicate with CR to check
  - Is the order on menu?
  - Does it have enough money to purchase it?
- The order should ONLY be made if those criteria are fulfilled. If any of those criteria aren’t fulfilled, the order is cancelled, and the SR returns to the starting position. The order ends.
- If the order is made, the SR should
  - Proceed to the starting point.
  - Wait for the time stated for that food in the "Menu.csv".
- When the time elapsed, the SR should proceed to "PickupTileSR" and inform the CR to pick up the food.
- Proceed back to the starting position and the order is completed. When all order is completed, the "Account.csv" file should have all the relevant data written to it (refer to [Section 4.4.](#44-accountcsv)).

You are required to design the state of the auto mode. You are free to add in and make any assumption as long as you fulfil the requirements stated. You should print out messages for each state that reflect the current state.

```
Director: Auto Mode starts
Customer 1: I am heading to order counter
Customer 1: Hi Staff, I want to order Latte
Staff: I am heading to order counter
Staff: Hi Customer 1, the price for Latte is 4 dollars
Customer 1: Hi Staff, I will buy it
Staff: Thanks for your order. It will be ready in 120 seconds
Staff: Hi customer 1, your Latte is ready, please proceed to pickup counter
Customer 1: I am heading to pickup counter
Customer 1: I got my Latte
Customer 1: I am returning to starting point
Staff: I am returning to starting point
Director: Order 1 complete
Customer 2: I am heading to order counter
Customer 2: Hi Staff, I want to order Sate
Staff: I am heading to order counter
Staff: Hi Customer 2, the price for Sate is 1000 dollars
Customer 2: Oops, I can't afford it. I will cancel the order
Customer 2: I am returning to starting point
Director: Order 2 complete
Customer 3: I am heading to order counter
Customer 3: Hi Staff, I want to order Cappuccino
Staff: I am heading to order counter
Staff: Hi Customer 3, the price for Cappuccino is 4.5 dollars
Customer 3: Hi Staff, I will buy it
Staff: Thanks for your order. It will be ready in 130 seconds
Staff: Hi Customer 3, your Cappuccino is ready, please proceed to pickup counter
Customer 3: I am heading to pickup counter
Customer 3: I got my Cappuccino
Customer 3: I am returning to starting point
Staff: I am returning to starting point
Director: Order 3 complete
Customer 4: I am heading to order counter
Customer 4: Hi Staff, I want to order Mocha
Staff: I am heading to order counter
Staff: Hi Customer 4, the price for Mocha is 4 dollars
Customer 4: Hi Staff, I will buy it
Staff: Thanks for your order. It will be ready in 150 seconds
Staff: Hi Customer 4, your Mocha is ready, please proceed to pickup counter
Customer 4: I am heading to pickup counter
Customer 4: I got my Mocha
Customer 4: I am returning to starting point
Staff: I am returning to starting point
Director: Order 4 complete
Customer 1: I am heading to order counter
Customer 1: Hi Staff, I want to order Lattea
Staff: I am heading to order counter
Staff: Hi Customer 1, oh no, we don't have Lattea in our menu
Customer 1: Oops, I will cancel the order
Customer 1: I am returning to starting point
Staff: I am returning to starting point
Director: Order 5 complete
Director: All orders are completed
Customer 1: My current balance is 6 dollars
Customer 2: My current balance is 15 dollars
Customer 3: My current balance is 25.5 dollars
Customer 4: My current balance is 11 dollars
```

### 3.3.4. Implementation of `StaffRobot` Class

Similar as the "BaseRobot" class, you must include all the member functions stated below. You are free to add in any member variables and helper functions for your approach.

<table>
  <tr>
    <th>Method</th>
    <th>Description</th>
    <th>Usage</th>
    <th>Output</th>
    <th>Exception</th>
  </tr>
  <tr>
    <td><code>StaffRobot()</code></td>
    <td>Constructor for staff robot</td>
    <td><code>StaffRobot robot;</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>~StaffRobot()</code></td>
    <td>Destructor</td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void run() override</code></td>
    <td>
      A virtual function. When this is called, the SR enter the initial state and execute other
      states
    </td>
    <td>
      <code>StaffRobot robot;<br />robot.run();</code>
    </td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void remoteControl() override</code></td>
    <td>A virtual function that executes remote control mode.</td>
    <td><code>StaffRobot robot;<br/>robot.remoteControl();</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void autoMode() override</code></td>
    <td>A virtual function that executes the auto mode.</td>
    <td><code>StaffRobot robot;<br/>robot.autoMode();</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
</table>

### 3.3.5. Implementation of `CustomerRobot` Class

<table>
  <tr>
    <th>Method</th>
    <th>Description</th>
    <th>Usage</th>
    <th>Output</th>
    <th>Exception</th>
  </tr>
  <tr>
    <td><code>CustomerRobot()</code></td>
    <td>Constructor for customer robot</td>
    <td><code>CustomerRobot robot;</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>~CustomerRobot()</code></td>
    <td>Destructor</td>
    <td></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void run() override</code></td>
    <td>
      A virtual function. When this is called, the CRs enter the initial state and execute other
      states
    </td>
    <td>
      <code>CustomerRobot robot;<br />robot.run();</code>
    </td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void remoteControl() override</code></td>
    <td>A virtual function that executes remote control mode.</td>
    <td><code>CustomerRobot robot;<br/>robot.remoteControl();</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
  <tr>
    <td><code>virtual void autoMode() override</code></td>
    <td>A virtual function that executes the auto mode.</td>
    <td><code>CustomerRobot robot;<br/>robot.autoMode();</code></td>
    <td>None</td>
    <td>None</td>
  </tr>
</table>

## 3.4. Director Robot

The Director Robot is the "behind the scene" robot. It provides the user interface and commands the relevant robot based on the user’s input. You can use ANY method for this controller, including Object-Orientated Programming or Procedural Programming. The implementation for this section should be stored in `z1234567DirectorRobot.cpp`, replacing `z1234567` with your zID.

### 3.4.1. Initial Mode

When the simulation starts, the controller attached to the DR should enter the initial mode:
When the simulation starts, the controller attached to the DR should enter the initial mode:

1. The controller should print out the command guide in the console:

```
Director: This is a simulation for MTRN2500 Cafe.
Director: Press [I] to reprint the commands.
Director: Press [R] to remote control a robot.
Director: Press [A] to enter the auto mode.
Director: Press [Q] to quit all controllers.
```

2. After printing, the DR should wait for the user input. The user will need to click on the simulation window and press a key.

### 3.4.2. User Input

#### 3.4.2.1. User Input `I` or `i`

If the user inputs `I` or `i`, the controller should reprint the command guide to the console:

```
Director: This is a simulation for MTRN2500 Cafe.
Director: Press [I] to reprint the commands.
Director: Press [R] to remote control a robot.
Director: Press [A] to enter the auto mode.
Director: Press [Q] to quit all controllers.
```

#### 3.4.2.2. User Input `R` or `r`

If the user inputs `R` or `r`, the controller should request the user to input a number between 1 to 5:

1. Purple Robot (Customer1)
2. White Robot (Customer2)
3. Gold Robot (Customer3)
4. Green Robot (Customer4)
5. Black Robot (Staff)

```
Director: Press [Q] to quit all controllers.
Director: Please select the robot to control remotely:
Director: Press [1] to control the Purple Robot (Customer1).
Director: Press [2] to control the White Robot (Customer2).
Director: Press [3] to control the Gold Robot (Customer3).
Director: Press [4] to control the Green Robot (Customer4).
Director: Press [5] to control the Black Robot (Staff).
```

1. If the user inputs a number between 1 to 5, the controller should request the relevant robot to proceed to the remote control mode.
2. If the user inputs anything else, the controller should print a message `Command not found.` and then reprint the initial command guide:

```
Director: Press [5] to control the Black Robot (Staff).
Director: Command not found.
Director: This is a simulation for MTRN2500 Cafe.
Director: Press [I] to reprint the commands.
Director: Press [R] to remote control a robot.
Director: Press [A] to enter the auto mode.
Director: Press [Q] to quit all controllers.
```

#### 3.4.2.3. User Input `A` or `a`

If the user inputs "A" or "a", the controller should request the SR and the four CRs to execute the auto mode.

#### 3.4.2.4. User Input `Q` or `q`

If the user inputs "Q" or "q", the controller should ask all the robot controllers (including itself) to quit the simulation.

#### 3.4.2.5. User Input Undefined

If the user input is beyond what is defined above, the controller should print a message `"Command not found."` and then reprint the initial command guide:

```
Director: Press [Q] to quit all controllers.
Director: Command not found.
Director: This is a simulation for MTRN2500 Cafe.
Director: Press [I] to reprint the commands.
Director: Press [R] to remote control a robot.
Director: Press [A] to enter the auto mode.
Director: Press [Q] to quit all controllers.
```

## 3.5. State Diagram

Design a state diagram that:
- Satisfies the specification outlined in section 3.3.3 Auto Mode.
- Makes sense i.e. does not contain any unusual symbols and drawings that a state diagram usually does not have.
- Has labelled states and transitions.
- Has fully defined transitions i.e. transitions are labelled with clear conditionals for branching/execution.

# 4. CSV Files

## 4.1. `Starting.csv`

The `Starting.csv` tabulates the `RobotID` and their starting cash amount. These are the starting cash that each robot will have.

To reiterate from the previous section, the robot that corresponds to the relevant `RobotID` is shown below.

| `RobotID` | Robot               |
| :-------: | ------------------- |
|     1     | Purple Robot        |
|     2     | White Robot         |
|     3     | Gold Robot          |
|     4     | Green Robot         |
|     5     | (Staff) Black Robot |

![Starting.csv](README/starting_csv.png)

## 4.2. `Order.csv`

The `Order.csv` tabulates the `RobotID` and the food that needs to be ordered. The CRs will have to order the food from the SR in sequence.

The SR or CRs should not have access to the "Order.csv" file.

![Order.csv](README/order_csv.png)

## 4.3. `Menu.csv`

The `Menu.csv` records the food available, preparation time in seconds, and cost.

![Menu.csv](README/menu_csv.png)

## 4.4. `Account.csv`

At the end of auto mode, the SR should produce an `Account.csv` that records the valid orders, including the starting balance of the SR, the order number counter, the item of the order, the customer that makes the order and the account balance after the order is made.

The DR or CRs should not have access to the `Account.csv` file.

![Accounts.csv](README/account_csv.png)

# 5. Specifications and Hints

## 5.1. Specifications

- Ensure you are using **Webots 2021B**.
- The Webots world file used for assessment will be the same as the one provided at the release of the assignment. You **should NOT change the world file**.
- You are NOT allowed to change the robot except the "controller" section where you include your controller file.
- You should separate implementations and interfaces into multiple cpp and hpp files.
- Your controller should **use C++14** as C++ standard version.
- The movement of any robot should be achieved **ONLY** by motor and sensors.
- Throughout the program, the absolute speed of the motors should NOT exceed the maximum speed as specified at any time.
- Example csv files will be provided for your reference. However, you should expect **MODIFIED** csv files for assessment.

## 5.2. Hints

- Consult the lecturer/demonstrators if you are unclear about ANYTHING
- When asking for help, please follow the forum etiquette
- Have a careful thought on the whole structure of the program and the flow of the program before coding. Refer to state diagram tutorial
- Divide the problem into sub-problems and solve them one by one
- Functionality and correctness are the basis of everything (you won’t get paid if your program does not do the job, even when you write beautiful code)
- However, it is also essential to follow the [style guide](https://gitlab.com/dennuguyen/mtrn2500-2022t3/-/blob/master/style-guide.md) as much as possible when the functionality is achieved (style matters!)

# 6. Assessment

## 6.1. Progress Check

The progress check for this assignment will be on your **tutorial 2 in week 9**. It contributes **1%** for this assignment and will be in a "pass or fail" mode. To pass the progress check, you are required to:
- Complete section 3.3.1.
- Complete section 3.3.2.
- Complete section 3.5.

## 6.2. Marking Criteria

This assignment will contribute **35%** to your final mark. The marks will include two components: **25 marks** for the **functionality** of your program **3%** is on the state diagram, **1%** is on the progress check,  and **6%** is on the style (quality).

### 6.2.1. Functionality (25 Marks)

The functionality of your program will be marked by following the criteria below:

<table>
  <tr>
    <th>Task</th>
    <th>Robot</th>
    <th>Mode</th>
    <th>Marking</th>
  </tr>
  <tr>
    <td rowspan="3">Interface and File Processing</td>
    <td rowspan="3">DR (3)</td>
    <td rowspan="1">IN (2)</td>
    <td>
      <ul>
        <li>(1) Correctly printing the command guide</li>
        <li>(1) Executing the correct command for each input</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>AT (1)</td>
    <td>
      <ul>
        <li>(1) Correctly reading and parsing <code>Order.csv</code></li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Penalties</td>
    <td>
      <span>0 for the section if found:</span>
      <ul>
        <li>No No communication between robots, or</li>
        <li>DR gets access to <code>Starting.csv</code> from some tasks, or</li>
        <li>DR or CRs output the <code>Accounts.csv</code></li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Base Robot</td>
    <td>BR (12)</td>
    <td>N/A</td>
    <td>
      <ul>
        <li>(2) Constructor</li>
        <li>(0.5) Destructor</li>
        <li>(3) <code>void move(double, double, double)</code> function</li>
        <li>
          (1.25) <code>void sendMessage(const std::string&, int)</code> function
        </li>
        <li>(1.25) <code>std::string receiveMessage()</code> function</li>
        <li>
          (1)
          <code>friend std::ostream& operator<<(std::ostream&, BaseRobot const&)</code>
          function
        </li>
        <li>(1) <code>virtual void run()</code> function</li>
        <li>(1) <code>virtual void remoteControl()</code> function</li>
        <li>(1) <code>virtual void autoMode()</code> function</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td rowspan="3">Motion and File Processing</td>
    <td rowspan="3">SR (5)</td>
    <td>RC(1)</td>
    <td>
      <ul>
        <li>(1) Executing the remote control correctly</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>AT(4)</td>
    <td>
      <span>(4) Executing the auto mode correctly, including:</span>
      <ul>
        <li>
          (1) Proceeding to the correct location every time (order tile and
          pickup tile, robots must be fully within the specified tiles)
        </li>
        <li>(1) Presenting the correct workflow</li>
        <li>
          (2) Outputting to the correct <code>Accounts.csv</code> at the end of
          auto mode
        </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Penalties</td>
    <td>
      <span>0 for this section if found:</span>
      <ul>
        <li>The constructor fails to compile</li>
        <li>
          The <code>run()</code>, <code>remoteControl()</code>,
          <code>autoMode()</code> functions are not implemented
        </li>
      </ul>
      <span>0 for the AT section if found:</span>
      <ul>
        <li>No communication between robots, or</li>
        <li>SR or CRs get access to <code>Order.csv</code> for some tasks</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td rowspan="3">Motion and File Processing</td>
    <td rowspan="3">CRs (5)</td>
    <td>RC (1)</td>
    <td>
      <ul>
        <li>(1) Executing the remote control mode correctly</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>AT(4)</td>
    <td>
      <span>(4) Executing auto mode correctly, including:</span>
      <ul>
        <li>
          (1) Proceeding to the correct location every time (order tile and
          pickup tile, robots must be fully within the specified tiles)
        </li>
        <li>(1) Presenting the correct workflow</li>
        <li>(2) Outputting the correct balance at the end of auto mode</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Penalties</td>
    <td>
      <span>0 for this section if found:</span>
      <ul>
        <li>The constructor fails to compile</li>
        <li>
          The <code>run()</code>, <code>remoteControl()</code>,
          <code>autoMode()</code> functions are not implemented
        </li>
      </ul>
      <span>0 for the AT section if found:</span>
      <ul>
        <li>No communication between robots, or</li>
        <li>
          SR or CRs get access to <code>Order.csv</code> for some tasks, or
        </li>
        <li>DR or CRs get access to <code>Menu.csv</code> for some tasks</li>
      </ul>
    </td>
  </tr>
</table>

### 6.2.2. Quality

The quality of your code will be marked by following the criteria below. Note that the mark for this part will be capped by a proportional mark of your performance in the functionality. For example, if your mark for functionality is `X` (out of 25) marks, then the maximum mark you can get for the quality is `X / 25 * 6` marks (make sure your program works first!).


<table>
    <tr>
        <th>Criteria</th>
        <th>Weight</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>C++ Style</td>
        <td>6</td>
        <td>There will be marks for using C++ style such as using STL algorithms, avoiding C-style code, invalidating resources after move, etc. Generally good programming practices such as no code duplication, consistent indentation and braces, consistent naming style, meaningful function and variable names, etc.</td>
    </tr>
</table>

### 6.2.3. State Diagram
The state diagram will be marked by following the criteria below.
<table>
    <tr>
        <th>Criteria</th>
        <th>Weight</th>
        <th>Description</th>
    </tr>
    <tr>
      <td>State Diagram</td>
      <td>3</td>
      <td>State diagram satisfies the robot behaviour requirements. State diagram is sensical (i.e. no unusual symbols and drawings). States and transitions are labelled. Transitions are fully defined i.e. have clear conditions for execution.</td>
        </tr>

</table>

## 6.3. Submission

You should zip your Webots project, name it as `MTRN2500_z1234567.zip` where `z1234567` is your zID, and submit it via Moodle.

Your submission should contain all the files required to compile and run your program as well as your state diagram.
 - BaseRobot header and source files
 - CustomerRobot header, source and main files (as relevant)
 - StaffRobot header, source and main files (as relevant)
 - DirectorRobot header, source and main files (as relevant)
 - Makefiles for all controllers
 - State diagram in pdf format

A *suggested* submission folder structure would look like this.

```
z1234567_MTRN2500
├── Account.csv
├── Menu.csv
├── Order.csv
├── Starting.csv
├── z1234567StateDiagram.pdf
├── controllers
│   ├── BaseRobotMain
│   │   ├── z1234567BaseRobot.cpp
│   │   ├── z1234567BaseRobot.hpp
│   │   ├── helper.cpp (If Applicable)
│   │   ├── helper.hpp (If Applicable)
│   ├── CustomerRobotMain
│   │   ├── CustomerRobotMain.cpp
│   │   ├── z1234567CustomerRobot.cpp
│   │   ├── z1234567CustomerRobot.hpp
│   │   ├── helper.cpp (If Applicable)
│   │   ├── helper.hpp (If Applicable)
│   │   └── Makefile
│   ├── StaffRobotMain
│   │   ├── Makefile
│   │   ├── StaffRobotMain.cpp
│   │   ├── z1234567StaffRobot.cpp
│   │   ├── z1234567StaffRobot.hpp
│   │   ├── helper.cpp (If Applicable)
│   │   └── helper.hpp (If Applicable)
│   └── DirectorRobot
│       ├── Makefile
│       ├── z1234567DirectorRobot.cpp
│       ├── helper.cpp (If Applicable)
│       └── helper.hpp (If Applicable)
└── worlds
    └── MTRN2500.wbt
```

You should ensure your submitted files are the final version and self-contained. If the program fails to compile/execute, you could get zero for the relevant section (please clearly specify which operation system you used for the development, as sometimes you code runs correctly on one platform but fails on another).

## 6.4. Submission Deadline

Unless otherwise announced, the submission will be open from **21st November 2022**, and the submission deadline is on **1:00pm 28th November 2022 (Monday Week 12)**.

Submissions after the scheduled deadlines will incur a 5% penalty on your actual mark every 24 hours for 120 hours. After 120 hours, the submission will no longer be accepted.

## 6.5. Plagiarism

If you are unclear about the definition of plagiarism, please refer to [What is Plagiarism? | UNSW Current Students](https://www.student.unsw.edu.au/what-plagiarism#:~:text=Plagiarism%20at%20UNSW%20is%20using%20the%20words%20or,to%20accidentally%20copying%20from%20a%20source%20without%20acknowledgement.).

You could get **zero marks** for the assignment if you were found:

- Knowingly providing your work to anyone and it was subsequently submitted (by anyone), or
- Copying or submitting any other persons’ work, **including code from previous students of this course** (except general public open-source libraries/code). Please cite the source if you refer to open source code.

You will be notified and allowed to justify your case before such a penalty is applied.

# 7. Additional Resources

- Webots user guide: [https://cyberbotics.com/doc/guide/index](https://cyberbotics.com/doc/guide/index)
- Webots reference manual: [https://cyberbotics.com/doc/reference/index](https://cyberbotics.com/doc/reference/index)
- Webots official tutorial 1:
  [https://cyberbotics.com/doc/guide/tutorial-1-your-first-simulation-in-webots?tab-language=c++](https://cyberbotics.com/doc/guide/tutorial-1-your-first-simulation-in-webots?tab-language=c++)
- Webots official tutorial 4:
  [https://cyberbotics.com/doc/guide/tutorial-4-more-about-controllers?tab-language=c++](https://cyberbotics.com/doc/guide/tutorial-4-more-about-controllers?tab-language=c++)
- Webots robot node: [https://www.cyberbotics.com/doc/reference/robot?tab-language=c++](https://www.cyberbotics.com/doc/reference/robot?tab-language=c++)
- Webots motor node: [https://cyberbotics.com/doc/reference/motor?tab-language=c++](https://cyberbotics.com/doc/reference/motor?tab-language=c++)
- Webots keyboard node:
  [https://www.cyberbotics.com/doc/reference/keyboard?tab-language=c++](https://www.cyberbotics.com/doc/reference/keyboard?tab-language=c++)
- Webots virtual time: https://www.cyberbotics.com/doc/reference/glossary
- Webots receiver node: [https://cyberbotics.com/doc/reference/receiver?tab-language=c++](https://cyberbotics.com/doc/reference/receiver?tab-language=c++)
- Webots emitter node: [https://cyberbotics.com/doc/reference/emitter?tab-language=c++](https://cyberbotics.com/doc/reference/emitter?tab-language=c++)
- Webots virtual time: [https://www.cyberbotics.com/doc/reference/glossary](https://www.cyberbotics.com/doc/reference/glossary)
- MTRN2500 Style Guide: [https://gitlab.com/dennuguyen/mtrn2500-2022t3/-/blob/master/style-guide.md](https://gitlab.com/dennuguyen/mtrn2500-2022t3/-/blob/master/style-guide.md)

// File:          CustomerRobotMain.cpp
// Date:
// Description:
// Author:
// Modifications:
/////////////////////////////
// TODO: Change the include list
//

////////////////////////////////////

int main(int argc, char **argv) {
    // You are not allowed to modify this main function other than
    // changing the include list
    CustomerRobot robot;
    robot.run();
    return 0;
}
